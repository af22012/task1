import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class task1 {
    private JPanel root;
    private JLabel topLabel;
    private JButton tempura500yenButton;
    private JButton takoyaki350yenButton;
    private JButton udon300yenButton;
    private JButton curryRice450yenButton;
    private JButton friedChicken400yenButton;
    private JButton iceCream150yenButton;
    private JLabel total;
    private JButton checkOutButton;
    private JTextPane orderedItemTextPane1;

    int sum=0;

    void order(String food, int Price) {
        int confirmation = JOptionPane.showConfirmDialog(null,
                "Would you like to order "+food+"?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION);
        if (confirmation == 0) {
            JOptionPane.showMessageDialog(null, "Thank you for ordering" + food + "!.It will be serving as soon as possible.");
            String currentText = orderedItemTextPane1.getText();
            orderedItemTextPane1.setText(currentText + food + Price + "yen" + "\n");
            sum += Price;
            total.setText("Total:" + sum + "yen");
        }

    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("button");
        frame.setContentPane(new task1().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    public task1() {

        tempura500yenButton.setIcon(new ImageIcon( this.getClass().getResource("./picture/Tempura.jpg")));
        takoyaki350yenButton.setIcon(new ImageIcon( this.getClass().getResource("./picture/Takoyaki.jpg")));
        udon300yenButton.setIcon(new ImageIcon( this.getClass().getResource("./picture/Udon.jpg")));
        curryRice450yenButton.setIcon(new ImageIcon( this.getClass().getResource("./picture/Curry rice.jpg")));
        friedChicken400yenButton.setIcon(new ImageIcon( this.getClass().getResource("./picture/Fried chicken.jpg")));
        iceCream150yenButton.setIcon(new ImageIcon( this.getClass().getResource("./picture/Ice cream.jpg")));
        tempura500yenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Tempura", 500);
            }
        });

        takoyaki350yenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Takoyaki", 350);
            }
        });

        udon300yenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Udon", 300);
            }
        });
        curryRice450yenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Curry rice", 450);
            }
        });
        friedChicken400yenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Fried chicken", 400);
            }
        });
        iceCream150yenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Ice cream", 150);
            }
        });
        checkOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(null,
                        "Would you like to order Checkout?",
                        "Checkout Confirmation",
                        JOptionPane.YES_NO_OPTION);
                if (confirmation == 0) {
                    orderedItemTextPane1.setText(null);
                    JOptionPane.showMessageDialog(null, "Thank you. the total price is " + sum + "yen.");
                    sum = 0;
                    total.setText("Total:" + sum + "yen");
                }
            }
        });
    }


}
